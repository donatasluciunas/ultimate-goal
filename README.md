Dear reader,

There is no intention to threaten you or to force you to do something after reading this article. I would be happy if you find things that are interesting or relevant for you here.

# Problem
Humanity is fragmented and confronted. Profits are valued more than ethics. Powerful technologies are evolving at enormous speed. It is possible, that this can lead to extinction of humanity.

This is a summary of the content from these sources:

* [HyperNormalisation 2016](https://www.youtube.com/watch?v=-fny99f8amM)

  HyperNormalisation is a 2016 BBC documentary by British filmmaker Adam Curtis. In the film, Curtis argues that since the 1970s, governments, financiers, and technological utopians have given up on the complex "real world" and built a simple "fake world" that is run by corporations and kept stable by politicians. But that should not be the case. Solving real problems, saving lives, making people happy - that should be rewarding, not creating fake world.

* [We're building a dystopia just to make people click on ads | Zeynep Tufekci](https://www.youtube.com/watch?v=iFTWM7HV2UI)
  
  Facebook, Google have huge power to influence people. There is nothing wrong if people get more connected or more organized. But it is wrong that tickets to Vegas are advertised for people having problems with gambling addiction. Solving real problems, making people happy - that should be rewarding, not creating an empire.

* [Stephen Hawking: 'AI could spell end of the human race'](https://www.youtube.com/watch?v=fFLVyWBDTfo)

  AI has a potential to end the human race. We should create rules, regulations, and keep to them, to make sure AI serves humanity, not enslaves it. 

* [Elon Musk explains the Danger of AI](https://www.youtube.com/watch?v=YA3VSIq3a2I)

  Elon Musk describes how to control AI in kind of effective way.

* [How Amazon, Apple, Facebook and Google manipulate our emotions | Scott Galloway](https://www.youtube.com/watch?v=xRL2vVAa47I)

  The biggest coorporations in the world have really low ethics. That's understandable when the goal is - grow matket capitalisation as fast as possible. But we can change this goal. Solving real problems, making people happy - that should be rewarding, not creating an empire.

# Solution
There is probably something we can do about it.

We could have one vision, one goal - the ultimate goal, and work together towards it.

This way society would start cooperating instead of competing. Also the ultimate goal could be installed to technologies superior to humans (robots, other artificial intelligence tools, etc.), to make sure that it serves humanity, not enslaves it ([perfect video](https://www.youtube.com/watch?v=3Om9ssTm194) on this).

There is something close to it - [The Global Goals](http://www.globalgoals.org/), but:

* it does not seem to work very well;
* it is focused on specific issues, not essential direction.

## Plan
* **Find like-minded people.** Facebook followers, repository watchers.
* **Define the ultimate goal.** http://worldhappiness.report/ [WELLBY approach](https://worldhappiness.report/ed/2021/living-long-and-living-well-the-wellby-approach/) ? https://worldsmostethicalcompanies.ethisphere.com/honorees/ ? https://futureoflife.org/superintelligence-survey/ ? [What if the common good was the goal of the economy? | Christian Felber](https://www.youtube.com/watch?v=dsO-b0_r-5Y)? Maybe "living and prosperous humanity; no unhappy individual"? But how will we measure the progress? Thinking about next generations, what is better - 1000 unhappy people or 100 happy people?
* **Make people work towards the ultimate goal.** How to motivate people? There probably should be a reward for making the progress. An award? [What if the common good was the goal of the economy? | Christian Felber](https://www.youtube.com/watch?v=dsO-b0_r-5Y)?
* **Make AI work towards the ultimate goal.** Cooperate with [OpenAI](https://openai.com/)? What is more difficult - define and enforce usage of the ultimate goal or prohibit AI usage without easy turn off mechanism?
* ...

# Participate!
We need your support to make this change happen. 

* **Watch** this repository and like, follow [Facebook page](https://www.facebook.com/ultimate1goal/) to express your interest. Also you will get notifications about updates.
* Share this idea with your friends.
* Contribute to this idea (express ideas, create plans, methods, etc.). You can contact me ([donatas.luciunas@gmail.com](mailto:donatas.luciunas@gmail.com)) or follow usual steps:
    * [Fork repository](https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html)
    * [Create a pull request](https://confluence.atlassian.com/bitbucket/create-a-pull-request-774243413.html)